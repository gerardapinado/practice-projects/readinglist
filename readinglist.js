let msg = {
    animal: ["fox", "dog"],
    action: ["jumps"],
    trait: ["lazy", "quick"],
    size: "big",
    color: "brown"
}

let func = obj => {
    let {animal, action, trait, size, color} = obj
    return `The ${trait[1]} ${color} ${animal[0]} ${action} over the ${trait[0]} ${animal[0]}`
}

console.log(func(msg))